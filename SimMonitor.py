import sys
from SimCfg import *

class SimMonitor():
    def __init__(self, env, simcore):
        self.env = env
        self.simcore = simcore

        self.n_flows = 0
        self.n_legit_flows = 0
        self.n_forged_flows = 0
        self.n_packet_in = 0
        self.n_legit_packet_in = 0
        self.n_forged_packet_in = 0
        self.n_rej_legit_flows = 0
        self.n_pass_legit_flows = 0
        self.n_rej_forged_flows = 0
        self.n_pass_forged_flows = 0

        self.n_filter_input_flows = 0
        self.n_filter_input_legit_flows = 0
        self.n_filter_input_forged_flows = 0
        self.n_filter_output_flows = 0
        self.n_filter_output_legit_flows = 0
        self.n_filter_output_forged_flows = 0
        self.n_filter_drop_flows = 0
        self.n_filter_drop_legit_flows = 0
        self.n_filter_drop_forged_flows = 0

        self.total_legit_activation_delay = 0.0

        self.sw_load_dict = {k:0 for k in self.simcore.topo.nodes()}
        self.amu_load_dict = {k:0 for k in self.simcore.topo.nodes()}

        self.file_monitor = open(FN_MONITOR, 'w')

    def print_monitor(self):
        self.file_monitor.write("Time %-10.6f:" %(self.env.now))
        self.file_monitor.write("Total flows %d; (Total/pass/rej) legit flows: %d/%d/%d; forged flows: %d/%d/%d; (Total/legit/forged) packet_in msgs: %d/%d/%d\n" \
                                %(self.n_flows, \
                                  self.n_legit_flows, self.n_pass_legit_flows, self.n_rej_legit_flows, \
                                  self.n_forged_flows, self.n_pass_forged_flows, self.n_rej_forged_flows, \
                                  self.n_packet_in, self.n_legit_packet_in, self.n_forged_packet_in) )
        self.file_monitor.write("Smart filter (Input/output/dropped) flows: %d/%d/%d; (Input/output/dropped) legit flows: %d/%d/%d; (Input/output/dropped) legit flows: %d/%d/%d\n" \
                                %(self.n_filter_input_flows, self.n_filter_output_flows, self.n_filter_drop_flows, \
                                  self.n_filter_input_legit_flows, self.n_filter_output_legit_flows, self.n_filter_drop_legit_flows, \
                                  self.n_filter_input_forged_flows, self.n_filter_output_forged_flows, self.n_filter_drop_forged_flows) )

        #print "Per-switch packet_in loading: %s" %(self.sw_load_dict)
        #print "Per-AMU packet_in loading: %s" %(self.amu_load_dict)

        #for k in self.sw_load_dict:
        #   self.sw_load_dict[k] = 0
        #   self.amu_load_dict[k] = 0

        #print "Per-switch TCAM occupancy: %s" %( {k:self.simcore.switches[k].tcam_cnt for k in self.simcore.topo.nodes()} )
        #print "Per-AMU table occupancy: %s" %( {k:self.simcore.amus[k].tcam_cnt for k in self.simcore.topo.nodes()} )

        self.file_monitor.write("Average activation delay (per completed flow request): %f\n\n" %(self.total_legit_activation_delay / float(self.n_pass_legit_flows) ))

        self.file_monitor.flush()

        sys.stdout.flush()

        print


    def print_proc(self):
        while True:
            yield self.env.timeout(PERIOD_MONITOR_PRINT)
            self.print_monitor()