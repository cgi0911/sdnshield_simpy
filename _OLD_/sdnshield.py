#!/usr/bin/python

# -----------------------------------------------------------------------------
#
# Imports
#
# -----------------------------------------------------------------------------

import simpy
import random
import netaddr
import os
import networkx as nx
import csv
import time

from guppy import hpy

# -----------------------------------------------------------------------------
#
# Constant Parameters
#
# -----------------------------------------------------------------------------
FN_NODES = "./topologies/spain/nodes.csv"
FN_LINKS = "./topologies/spain/links.csv"

SIM_TIME = 1.0
SHOW_FULL_FLOW_INFO = False

TCAM_SIZE = 10

AVG_INTARR_TIME = 0.005
AVG_FLOW_DURATION = 1.0
SW_PROC_TIME_FLOWARRIV = 0.015
SW_PROC_TIME_FLOWMOD = 0.025
CTRL_PROC_TIME_ROUTING = 0.0008
RETRANS_TIMEOUT = 3.0
PROP_DELAY1 = 1e-4
PROP_DELAY2 = 2e-4
IDLE_TIMEOUT = 1.0

SW_OFA_Q_LEN = 1000
CTRL_INPUT_Q_LEN = 10000
MAX_N_FLOWREQS = 3

PROT_DICT = {1:'icmp', 6:'tcp', 17:'udp'}

# -----------------------------------------------------------------------------
#
# Classes
#
# -----------------------------------------------------------------------------
class SimMsg_FlowMod(object):
    def __init__(self, env, flow):
        self.env = env
        self.type = "flow_mod"
        self.flowobj = flow
        self.installed = self.env.event()


    def __str__(self):
        return "flow_mod(Flow #%d)" %(self.flowobj.fid)


    def __repr__(self):
        return __str__(self)


class SimMsg_PacketIn(object):
    def __init__(self, env, flow):
        self.env = env
        self.type = "packet_in"
        self.flowobj = flow


    def __str__(self):
        return "packet_in(Flow #%d)" %(self.flowobj.fid)


    def __repr__(self):
        return __str__(self)


class SimFlow(object):
    # Class for a flow object and its related simpy processes.
    def __init__(self, env, simcore,
                 src_ip = netaddr.IPAddress(0), dst_ip = netaddr.IPAddress(0),
                 src_port = 0, dst_port = 0, prot = 0, fid = 0,
                 src_node = 'SW0', dst_node = 'SW1'):
        self.env = env
        self.simcore = simcore
        self.fid = fid
        self.path = []
        # 5-tuple
        self.src_ip = src_ip
        self.dst_ip = dst_ip
        self.src_port = src_port
        self.dst_port = dst_port
        self.prot = prot
        # Other parameters
        self.arrival_time = self.env.now
        self.duration = random.uniform(0.0, 2*AVG_FLOW_DURATION)
        self.activation_time = 0.0
        self.end_time = 0.0
        self.remove_time = 0.0
        self.n_flowreq = 0
        self.src_node = src_node
        self.dst_node = dst_node
        # Flags
        self.sw_ofa_processed = self.env.event()
        self.flow_mod_sent = self.env.event()
        self.route_decided = self.env.event()

        self.env.process(self.life_proc())


    def __str__(self):
        if SHOW_FULL_FLOW_INFO == True:
            return "Flow #%d: %s:%d -> %s:%d %s  src:%s  dst:%s" \
                   %(self.fid, self.src_ip, self.src_port,
                     self.dst_ip, self.dst_port, PROT_DICT[self.prot], self.src_node, self.dst_node)
        else:
            return "Flow #%d" %(self.fid)


    def __repr__(self):
        return self.__str__()


    def __del__(self):
        print "Time %10.6f: Flow #%d completes entire life cycle. Object destroyed." %(self.env.now, self.fid)


    def life_proc(self):
        """Process of a flow's entire lifecycle. It consists of three phases:
        1.
        2.
        3.
        """
        # ---------- Phase 1: Initiate a flow request. ----------
        flow_req_success = 0
        while self.n_flowreq <= MAX_N_FLOWREQS:
            # Reset the events
            self.sw_ofa_processed = self.env.event()
            self.route_decided = self.env.event()
            self.n_flowreq += 1
            print "Time %10.6f: Flow #%d sends request #%d" %(self.env.now, self.fid, self.n_flowreq)
            res = yield self.env.process(self.req_proc())

            if res == -1:
                yield self.env.timeout(RETRANS_TIMEOUT)
            else:
                flow_req_success = 1
                break

        if flow_req_success == 1:
            print "Time %10.6f: Flow #%d flow request successful" %(self.env.now, self.fid)
            # Go on with next stage
        else:
            print "Time %10.6f: Flow #%d flow request failed after %d retries" %(self.env.now, self.fid, MAX_N_FLOWREQS)
            self.env.exit(-1)


        # ---------- Phase 2: Flow activated (data transmission). ----------
        res = yield self.env.process(self.active_proc())

        # ---------- Phase 3: Remove flow entries after idle timeout. ----------
        res = yield self.env.process(self.idle_proc())

        # ---------- Final cleanup for a flow ----------
        self.env.exit(0)


    def req_proc(self):
        """Process of an OpenFlow flow request transaction.
        1.
        """
        src_sw = self.simcore.switches[self.src_node]

        # Phase 1: Put the flow arrival to the source switch's OFA queue.
        res = yield self.env.process(src_sw.put_flowarriv_proc(self))
        if res == -1:
            self.env.exit(-1)

        # Phase 2: Wait for the flow request to be processed by source switch's OFA.
        res = yield self.sw_ofa_processed
        if res == -1:
            print "Time %10.6f: Flow #%d flow request failed to be processed by OFA of switch %s." %(self.env.now, self.fid, self.src_node)
            self.env.exit(-1)
        elif type(res) == SimMsg_PacketIn:
            print "Time %10.6f: Flow #%d flow request processed by OFA of switch %s." %(self.env.now, self.fid, self.src_node)
            msg = res

        # Phase 3: Put the flow arrival to the central controller's input queue.
        ctrl = self.simcore.ctrl
        res = yield self.env.process(ctrl.put_proc(msg))
        if res == -1:
            self.env.exit(-1)

        # Phase 4: Wait for the controller's routing decision
        self.path = yield self.route_decided
        self.route_decided = self.env.event()

        # Phase 4: Wait for the controller to put flow_mod messages into switches' OFA queues.
        res = yield self.env.process(self.simcore.ctrl.flow_install_proc(self))
        if res == -1:
            print "Time %10.6f: Flow #%d failed to install entries to all path switches." %(self.env.now, self.fid)
            self.env.exit(-1)
        else:
            print "Time %10.6f: Flow #%d successfully installed entries to all path switches. Path is: %s." %(self.env.now, self.fid, self.path)

        self.env.exit(0)


    def active_proc(self):
        # Process of flow data transmission.
        print "Time %10.6f: Flow #%d starts transmission. Est. end time = %.6f." %(self.env.now, self.fid, self.env.now + self.duration)
        #Do some updates here. Throughputs, counters, etc.
        yield self.env.timeout(self.duration)
        print "Time %10.6f: Flow #%d ends transmission" %(self.env.now, self.fid)
        #Sum up some states here.
        self.env.exit(0)


    def idle_proc(self):
        # Process of flow idle timeout.
        #print "%-10.6f: flow #%d idle" %(self.env.now, self.fid)
        yield self.env.timeout(IDLE_TIMEOUT)

        for nd in self.path:
            tar_sw = self.simcore.switches[nd]
            tcam = tar_sw.tcam
            del tcam[self]

        print "Time %10.6f: Flow #%d ends idle timeout and removes entries from all path switches." %(self.env.now, self.fid)
        self.env.exit(0)


class SimSwitch(object):
    # Class for an OpenFlow switch object
    def __init__(self, env, name="SW0"):
        self.env = env
        self.name = name
        # Resources
        self.ofa_q = simpy.Store(env, capacity=SW_OFA_Q_LEN)       # Model an OpenFlow agent buffer
        self.tcam = {}        # Model a limited-size TCAM table. A dict hashed by 5-tuple.

        env.process(self.ofa_proc())


    def __str__(self):
        return "Switch '%s'" %(self.name)


    def __repr__(self):
        return self.__str__()


    def put_flowarriv_proc(self, flow):
        """Put flow arrival to the switch's OFA queue.
        Exit with -1 if fail to put (due to ofa_q overflow).
        """
        yield self.env.timeout(PROP_DELAY1)              # Model the propagation delay

        with self.ofa_q.put(flow) as put_req:
            print "Time %10.6f: %s trying to put the arrival of Flow #%d to ofa_q." %(self.env.now, self.name, flow.fid)
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue is full

        if not put_req in res:
            print "Time %10.6f: %s failed to put the flow arrival of Flow #%d to ofa_q." %(self.env.now, self.name, flow.fid)
            self.env.exit(-1)   # Return a failure flag
        else:
            print "Time %10.6f: %s successfully put the flow arrival of Flow #%d to ofa_q. Current queue occupancy = %d." \
                  %(self.env.now, self.name, flow.fid, len(self.ofa_q.items) )
            self.env.exit(0)    # Return a success flag


    def put_flowmod_proc(self, msg):
        """Put flow mod to the switch's OFA queue.
        Exit with -1 if fail to put (due to ofa_q overflow).
        """
        yield self.env.timeout(PROP_DELAY2)             # Model the propagation delay

        flow = msg.flowobj

        with self.ofa_q.put(msg) as put_req:
            print "Time %10.6f: %s trying to put the flow_mod message of Flow #%d to ofa_q." %(self.env.now, self.name, flow.fid)
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue is full

        if not put_req in res:
            print "Time %10.6f: %s failed to put the flow_mod message of Flow #%d to ofa_q." %(self.env.now, self.name, flow.fid)
            msg.installed.succeed(value=-1)
            self.env.exit(-1)   # Return a failure flag
        else:
            print "Time %10.6f: %s successfully put the flow_mod message of Flow #%d to ofa_q. Current queue occupancy = %d." \
                  %(self.env.now, self.name, msg.flowobj.fid, len(self.ofa_q.items) )
            self.env.exit(0)


    def ofa_proc(self):
        """Process of OpenFlow agent handling OpenFlow events.
        """
        while True:     # Continuously process events
            ofa_q_item = yield self.ofa_q.get()
            print "Time %10.6f: %s gets an event from ofa_q. Event = %s." %(self.env.now, self.name, ofa_q_item)
            if type(ofa_q_item) == SimFlow:
                yield self.env.timeout(SW_PROC_TIME_FLOWARRIV)
                msg = SimMsg_PacketIn(self.env, ofa_q_item)
                ofa_q_item.sw_ofa_processed.succeed(value=msg)
                # No further processing. Controller actions will be initiated by SimFlow object.
            elif type(ofa_q_item) == SimMsg_FlowMod:
                flow = ofa_q_item.flowobj
                node_name = self.name
                if len(self.tcam) >= TCAM_SIZE:
                    print "Time %10.6f: %s do not have TCAM space for new entry of Flow #%d." %(self.env.now, self.name, flow.fid)
                    ofa_q_item.installed.succeed(value=-1)
                    #flow.tcam_have_space[self.name].succeed(value=-1)
                else:
                    print "Time %10.6f: %s have TCAM space for new entry of Flow #%d." %(self.env.now, self.name, flow.fid)
                    yield self.env.timeout(SW_PROC_TIME_FLOWMOD)
                    self.tcam[flow] = 1
                    print "Time %10.6f: %s installed new entry of Flow #%d." %(self.env.now, self.name, flow.fid)
                    ofa_q_item.installed.succeed(value=0)


class SimCtrl(object):
    # Class for a centralized controller
    def __init__(self, env, simcore):
        self.env = env
        self.simcore = simcore
        # Resources
        self.input_q = simpy.Store(env, capacity=CTRL_INPUT_Q_LEN)

        self.env.process(self.routing_proc())


    def put_proc(self, msg):
        """Put packet_in message to the controller's input queue.
        """
        yield self.env.timeout(PROP_DELAY2)

        with self.input_q.put(msg) as put_req:
            print "Time %10.6f: Controller trying to put message to input_q. Message = %s" %(self.env.now, msg)
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue if full

        if not put_req in res:
            print "Time %10.6f: Controller failed to put message to input_q. Message = %s" %(self.env.now, msg)
            self.env.exit(-1)
        else:
            print "Time %10.6f: Controller successfully put message to input_q. Message = %s" %(self.env.now, msg)
            self.env.exit(0)


    def routing_proc(self):
        # Infinite loop that continuously pops flow requests and do routing calculation
        while True:
            packet_in_msg = yield self.input_q.get()
            flow = packet_in_msg.flowobj
            print "Time %10.6f: Controller gets a message from input_q. Message = %s." %(self.env.now, packet_in_msg)

            # Do the routing (shortest path only)
            yield self.env.timeout(CTRL_PROC_TIME_ROUTING)  # Model the processing
            src = flow.src_node
            dst = flow.dst_node
            path = self.simcore.path_db[(src, dst)][0]

            flow.route_decided.succeed(value=path)


    def flow_install_proc(self, flow):
        # Install flow_mod messages at each switch
        ev_list = []
        msg_list = []
        for nd in flow.path:
            tar_sw = self.simcore.switches[nd]
            msg = SimMsg_FlowMod(self.env, flow)
            ev_list.append(msg.installed) # Add 'installed' event to event list
            self.env.process(tar_sw.put_flowmod_proc(msg))  # Add a put_flowmod_proc

        res = yield simpy.events.AllOf(self.env, ev_list)

        if any(res[k] == -1 for k in res):
            # Remove those installed flows
            for nd in flow.path:
                if flow in self.simcore.switches[nd].tcam:
                    del self.simcore.switches[nd].tcam[flow]
            self.env.exit(-1)  # Return a successful flag
        else:
            self.env.exit(0)


class SimFlowGen(object):
    """ Class for flow generator. Can be instantiated to simulate DDoS attacks by generating forged flows.
    To forge a mixed attack, you can create multiple instances.
    """
    def __init__(self, env, simcore):
        self.env = env
        self.simcore = simcore
        self.curr_fid = 0       # Used to assign FIDs to flows
        self.node_list = sorted(self.simcore.topo.nodes())
        self.n_nodes = len(self.node_list)

        # Add process to env
        self.env.process(self.flowgen_proc())


    def flowgen_proc(self):
        # Wait until the specified attack start time
        while True:
            intarr_time = random.uniform(0, 2*AVG_INTARR_TIME)
            yield  self.env.timeout(intarr_time)
            self.gen_a_flow()


    def gen_a_flow(self):
        while True:
            src_ip = netaddr.IPAddress(random.randint(0, 2**32))
            dst_ip = netaddr.IPAddress(random.randint(0, 2**32))
            src_idx = hash(int(src_ip >> 16)) % self.n_nodes
            dst_idx = hash(int(dst_ip >> 16)) % self.n_nodes
            if (src_ip <> dst_ip and src_idx <> dst_idx):
                src_node = self.node_list[src_idx]
                dst_node = self.node_list[dst_idx]
                break

        while True:
            src_port = random.randint(0, 65535)
            dst_port = random.randint(0, 65535)
            if (src_port <> dst_port):
                break

        prot = 6 #TCP

        new_flow =   SimFlow(self.env, self.simcore, src_ip=src_ip, dst_ip=dst_ip,
                     src_port=src_port, dst_port=dst_port, prot=prot,
                     fid=self.curr_fid, src_node=src_node, dst_node=dst_node)
        #print "Time %10.6f: Flow generator send new flow. %s" %(self.env.now, new_flow)

        self.curr_fid += 1







class SimCore(object):
    """ Class for main process and entire simulation database (except those recorded in simpy.Environment).
    """
    def __init__(self, env):
        # Environment
        self.env = env
        # Topology (as an NetworkX undirected graph)
        self.topo = self.build_topo(fn_nodes=FN_NODES, fn_links=FN_LINKS)
        self.path_db = self.build_paths()
        # A dict for all active flows
        #self.flows = {}
        # A dict of switches
        self.switches = {}
        # Controller
        self.ctrl = None
        # Flow generator
        self.flowgen = None


    def add_ctrl(self):
        self.ctrl = SimCtrl(self.env, self)


    def add_switches(self):
        for node_name in self.topo.nodes():
            self.switches[node_name] = SimSwitch(self.env, name=node_name)


    def add_flowgen(self):
        self.flowgen = SimFlowGen(self.env, self)


    def build_topo(self, fn_nodes=FN_NODES, fn_links=FN_LINKS):
        """Build topology - read nodes and links from csv files and build topology info
        in the format of NetworkX undirected graph.
        """
        def dict_convert(myDict):
            # Each row of the CSV files is parsed into a row dictionary. Dict keys are
            # column names and values are assigned accordingly, but in string format.
            # This function converts strings into corresponding integers or float numbers.
            ret = {}
            for k in myDict:
                try:
                    ret[k] = int(myDict[k])
                except:
                    try:
                        ret[k] = float(myDict[k])
                    except:
                        ret[k] = myDict[k]
            return ret

        topo = nx.Graph()

        nodes_csv_reader = csv.DictReader(open(fn_nodes, 'rU'))
        links_csv_reader = csv.DictReader(open(fn_links, 'rU'))

        # Read nodes file row by row
        for rowdict in nodes_csv_reader:
            rowdict = dict_convert(rowdict)
            name = rowdict['name']
            topo.add_node(name)

        # Read links file row by row
        for rowdict in links_csv_reader:
            rowdict = dict_convert(rowdict)
            node1, node2 = rowdict['node1'], rowdict['node2']
            topo.add_edge(node1, node2)

        return topo


    def build_paths(self):
        """For each source/destination pair, pre-build its shortest path.
        """
        path_db = {}
        node_list = self.topo.nodes()

        for src in node_list:
            for dst in node_list:
                paths = []
                paths.append( nx.shortest_path(self.topo, src, dst) )
                path_db[(src, dst)] = paths

        return path_db



# -----------------------------------------------------------------------------
#
# Main Course
#
# -----------------------------------------------------------------------------
def main_course():
    random.seed(time.time())

    #h = hpy()
    # Instantiate simpy environment
    env = simpy.Environment()

    # Build simcore and add components.
    simcore = SimCore(env)
    simcore.add_ctrl()
    simcore.add_switches()
    simcore.add_flowgen()

    # Start simulation run
    env.run(until=SIM_TIME)

    #print h.heap()



if __name__ == "__main__":
    main_course()