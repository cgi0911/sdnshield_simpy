class SimMsg_FlowMod(object):
    def __init__(self, env, flow):
        self.env = env
        self.type = "flow_mod"
        #self.flowobj = flow
        self.fid = flow.fid
        # self.src_ip = flow.src_ip
        # self.dst_ip = flow.dst_ip
        # self.src_node = flow.src_node
        # self.dst_node = flow.dst_node
        # self.use_amu = flow.use_amu
        self.installed = self.env.event()



    def __str__(self):
        return "flow_mod(Flow #%d)" %(self.fid)


    def __repr__(self):
        return __str__(self)




class SimMsg_PacketIn(object):
    def __init__(self, env, flow, use_amu=False):
        self.env = env
        self.type = "packet_in"
        #self.flowobj = flow
        self.fid = flow.fid
        self.src_ip = flow.src_ip
        self.dst_ip = flow.dst_ip
        self.src_port = flow.src_port
        self.dst_port = flow.dst_port
        self.prot = flow.prot
        self.src_node = flow.src_node
        self.dst_node = flow.dst_node
        #self.use_amu = flow.use_amu
        self.use_amu = use_amu
        self.route_decided = flow.route_decided


    def __str__(self):
        return "packet_in(Flow #%d)" %(self.fid)


    def __repr__(self):
        return __str__(self)