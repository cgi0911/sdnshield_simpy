import simpy
import logging

from SimCfg import *
from SimFlow import *
from SimMsg import *
from SimMonitor import *

class SimAMU(object):
    # Class for an OpenFlow switch object
    def __init__(self, env, simcore, name="SW0"):
        self.env = env
        self.simcore = simcore
        self.monitor = simcore.monitor
        self.ctrl = simcore.ctrl
        self.name = name
        # Resources
        self.ofa_q = simpy.Store(env, capacity=AMU_OFA_Q_LEN)       # Model an OpenFlow agent buffer
        #self.tcam = {}        # Model a limited-size TCAM table. A dict hashed by 5-tuple.
        self.tcam_cnt = 0       # Record TCAM occupancy
        self.tcam_size = AMU_TABLE_SIZE

        env.process(self.ofa_proc())


    def __str__(self):
        return "AMU_%s" %(self.name)


    def __repr__(self):
        return self.__str__()


    def put_flowarriv_proc(self, flow):
        """Put flow arrival to the switch's OFA queue.
        Exit with -1 if fail to put (due to ofa_q overflow).
        """
        yield self.env.timeout(PROP_DELAY3)              # Model the propagation delay

        if self.simcore.op_mode == "FILTER":
            self.monitor.n_filter_input_flows += 1
            if flow.legit == True:      self.monitor.n_filter_input_legit_flows += 1
            else:                       self.monitor.n_filter_input_forged_flows += 1

            srcip_score = self.ctrl.ready_scoreboard_srcip.get_score(flow.src_ip)
            dstip_score = self.ctrl.ready_scoreboard_dstip.get_score(flow.dst_ip)
            srcport_score = self.ctrl.ready_scoreboard_srcport.get_score(flow.src_port)
            dstport_score = self.ctrl.ready_scoreboard_dstport.get_score(flow.dst_port)
            prot_score = self.ctrl.ready_scoreboard_prot.get_score(flow.prot)
            total_score = srcip_score + dstip_score + srcport_score + dstport_score + prot_score + self.ctrl.ready_scoreboard_srcip.levelscore
            if total_score <= self.ctrl.ready_cdf.thresh_score:   # Must be filtered out
                self.ctrl.working_mea_profile_srcip.add_item(flow.src_ip)
                self.ctrl.working_mea_profile_dstip.add_item(flow.dst_ip)
                self.ctrl.working_mea_profile_srcport.add_item(flow.src_port)
                self.ctrl.working_mea_profile_dstport.add_item(flow.dst_port)
                self.ctrl.working_mea_profile_prot.add_item(flow.prot)
                self.ctrl.working_cdf.add_item(total_score, "AMU_FILTER")
                # Monitoring
                self.monitor.n_filter_drop_flows += 1
                if flow.legit == True:      self.monitor.n_filter_drop_legit_flows += 1
                else:                       self.monitor.n_filter_drop_forged_flows += 1

                self.env.exit(-1)
            else:
                self.monitor.n_filter_output_flows += 1
                if flow.legit == True:      self.monitor.n_filter_output_legit_flows += 1
                else:                       self.monitor.n_filter_output_forged_flows += 1

        #if flow.legit == False:
        #    yield self.env.exit(-1)

        with self.ofa_q.put(flow) as put_req:
            self.monitor.amu_load_dict[self.name] += 1
            logging.debug("Time %10.6f: %s trying to put the arrival of Flow #%d to ofa_q." %(self.env.now, str(self), flow.fid))
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue is full

        if not put_req in res:
            logging.debug("Time %10.6f: %s failed to put the flow arrival of Flow #%d to ofa_q." %(self.env.now, str(self), flow.fid))
            self.env.exit(-1)   # Return a failure flag
        else:
            logging.debug("Time %10.6f: %s successfully put the flow arrival of Flow #%d to ofa_q. Current queue occupancy = %d." \
                          %(self.env.now, str(self), flow.fid, len(self.ofa_q.items)) )
            self.env.exit(0)    # Return a success flag


    def put_flowmod_proc(self, msg):
        """Put flow mod to the switch's OFA queue.
        Exit with -1 if fail to put (due to ofa_q overflow).
        """
        yield self.env.timeout(PROP_DELAY4)             # Model the propagation delay

        #flow = msg.flowobj

        with self.ofa_q.put(msg) as put_req:
            #self.monitor.sw_load_dict[self.name] += 1
            logging.debug("Time %10.6f: %s trying to put the flow_mod message of Flow #%d to ofa_q." %(self.env.now, str(self), msg.fid))
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue is full

        if not put_req in res:
            logging.debug("Time %10.6f: %s failed to put the flow_mod message of Flow #%d to ofa_q." %(self.env.now, str(self), msg.fid))
            msg.installed.succeed(value=-1)
            self.env.exit(-1)   # Return a failure flag
        else:
            logging.debug("Time %10.6f: %s successfully put the flow_mod message of Flow #%d to ofa_q. Current queue occupancy = %d." \
                          %(self.env.now, str(self), msg.fid, len(self.ofa_q.items)) )
            self.env.exit(0)


    def ofa_proc(self):
        """Process of OpenFlow agent handling OpenFlow events.
        """
        while True:     # Continuously process events
            ofa_q_item = yield self.ofa_q.get()
            logging.debug("Time %10.6f: %s gets an event from ofa_q. Event = %s." %(self.env.now, self.name, ofa_q_item))
            if type(ofa_q_item) == SimFlow:
                yield self.env.timeout(AMU_PROC_TIME_FLOWARRIV)
                msg = SimMsg_PacketIn(self.env, ofa_q_item, use_amu=True)
                ofa_q_item.sw_ofa_processed.succeed(value=msg)
                # No further processing. Controller actions will be initiated by SimFlow object.
            elif type(ofa_q_item) == SimMsg_FlowMod:
                #flow = ofa_q_item.flowobj
                node_name = self.name
                if self.tcam_cnt >= self.tcam_size:
                    logging.debug("Time %10.6f: %s do not have table space for new entry of Flow #%d." %(self.env.now, str(self), ofa_q_item.fid))
                    ofa_q_item.installed.succeed(value=-1)
                    #flow.tcam_have_space[self.name].succeed(value=-1)
                else:
                    logging.debug("Time %10.6f: %s have table space for new entry of Flow #%d." %(self.env.now, str(self), ofa_q_item.fid))
                    yield self.env.timeout(AMU_PROC_TIME_FLOWMOD)
                    #self.tcam[flow.fid] = 1
                    #self.tcam_cnt += 1  # Do not add yet. Wait for all on-path devices to return 0.
                    logging.debug("Time %10.6f: %s installed new entry of Flow #%d." %(self.env.now, str(self), ofa_q_item.fid))
                    ofa_q_item.installed.succeed(value=0)
