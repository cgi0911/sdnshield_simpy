import random
import netaddr
import logging
import gc

from SimCfg import *
from SimMsg import *
from SimMonitor import *

class SimFlow(object):
    # Class for a flow object and its related simpy processes.
    def __init__(self, env, simcore,
                 src_ip=netaddr.IPAddress(0), dst_ip=netaddr.IPAddress(0),
                 src_port=0, dst_port=0, prot=0, fid=0, duration=DEFAULT_FLOW_DURATION,
                 src_node='SW0', dst_node='SW1', legit=False, max_flowreqs=3):
        self.env = env
        self.simcore = simcore
        self.monitor = simcore.monitor
        self.fid = fid
        self.path = []
        # 5-tuple
        self.src_ip = src_ip
        self.dst_ip = dst_ip
        self.src_port = src_port
        self.dst_port = dst_port
        self.prot = prot
        # Other parameters
        self.arrival_time = self.env.now
        self.duration = duration
        self.activation_time = 0.0
        self.end_time = 0.0
        self.remove_time = 0.0
        self.n_flowreq = 0
        self.src_node = src_node
        self.dst_node = dst_node
        self.legit = legit
        self.max_flowreqs = max_flowreqs
        # Flags
        self.sw_ofa_processed = self.env.event()
        self.flow_mod_sent = self.env.event()
        self.route_decided = self.env.event()
        self.use_amu = False

        #self.env.process(self.life_proc())


    def __str__(self):
        if SHOW_FULL_FLOW_INFO == True:
            return "Flow #%d: %s:%d -> %s:%d %s  src:%s  dst:%s" \
                   %(self.fid, self.src_ip, self.src_port,
                     self.dst_ip, self.dst_port, PROT_DICT[self.prot], self.src_node, self.dst_node)
        else:
            return "Flow #%d" %(self.fid)


    def __repr__(self):
        return self.__str__()


    #def __del__(self):
    #    logging.info("Time %10.6f: Flow #%d completes entire life cycle. Object destroyed." %(self.env.now, self.fid))


    def life_proc(self):
        """Process of a flow's entire lifecycle. It consists of three phases:
        1.
        2.
        3.
        """

        flow_req_success = 0
        self.monitor.n_flows += 1
        if self.legit == True:
            self.monitor.n_legit_flows += 1
        else:
            self.monitor.n_forged_flows += 1

        # ---------- Phase 1: Initiate a flow request. Can retry several times. ----------
        self.arrival_time = self.env.now

        while self.n_flowreq <= self.max_flowreqs:
            # Reset the events
            self.sw_ofa_processed = self.env.event()
            self.route_decided = self.env.event()
            self.n_flowreq += 1
            logging.info("Time %10.6f: Flow #%d sends request - #%d try" %(self.env.now, self.fid, self.n_flowreq))

            self.monitor.n_packet_in += 1
            if self.legit == True:
                self.monitor.n_legit_packet_in += 1
            else:
                self.monitor.n_forged_packet_in += 1

            res = yield self.env.process(self.req_proc())

            if res == -1:
                self.n_flowreq += 1
                yield self.env.timeout(RETRANS_TIMEOUT)
            else:
                flow_req_success = 1
                break

        if flow_req_success == 1:
            if self.legit == True:
                self.monitor.n_pass_legit_flows += 1
            else:
                self.monitor.n_pass_forged_flows += 1
            logging.info("Time %10.6f: Flow #%d flow request successful" %(self.env.now, self.fid))
            # Go on with next stage
        else:
            if self.legit == True:
                self.monitor.n_rej_legit_flows += 1
            else:
                self.monitor.n_rej_forged_flows += 1
            logging.info("Time %10.6f: Flow #%d flow request rejected after %d retries" %(self.env.now, self.fid, MAX_N_FLOWREQS))
            #print "Time %10.6f: Flow #%d flow request rejected after %d retries" %(self.env.now, self.fid, MAX_N_FLOWREQS)
            self.env.exit(-1)


        # ---------- Phase 2: Flow activated (data transmission). ----------
        self.activation_time = self.env.now
        if self.legit == True:
            self.monitor.total_legit_activation_delay += self.activation_time - self.arrival_time
        res = yield self.env.process(self.active_proc())

        # ---------- Phase 3: Remove flow entries after idle timeout. ----------
        res = yield self.env.process(self.idle_proc())

        # ---------- Final cleanup for a flow ----------
        self.env.exit(0)


    def req_proc(self):
        """Process of an OpenFlow flow request transaction.
        1.
        """
        src_sw = self.simcore.switches[self.src_node]
        src_amu = self.simcore.amus[self.src_node]

        # Phase 1: Put the flow arrival to the source switch's OFA queue.
        if self.simcore.op_mode == "NORMAL":
            res = yield self.env.process(src_sw.put_flowarriv_proc(self))
        elif self.simcore.op_mode == "SUSTAIN" or self.simcore.op_mode == "FILTER":
            self.use_amu = True
            res = yield self.env.process(src_amu.put_flowarriv_proc(self))

        if res == -1:
            self.env.exit(-1)

        # Phase 2: Wait for the flow request to be processed by source switch's OFA.
        res = yield self.sw_ofa_processed
        if res == -1:
            logging.debug("Time %10.6f: Flow #%d flow request failed to be processed by OFA of switch or AMU at %s." %(self.env.now, self.fid, self.src_node))
            self.env.exit(-1)
        elif type(res) == SimMsg_PacketIn:
            logging.debug("Time %10.6f: Flow #%d flow request processed by OFA of switch/AMU %s. use_amu = %s" %(self.env.now, self.fid, self.src_node, self.use_amu))
            msg = res

        # Phase 3: Put the flow arrival to the central controller's input queue.
        res = yield self.env.process(self.simcore.ctrl.put_proc(msg))
        if res == -1:
            self.env.exit(-1)

        # Phase 4: Wait for the controller's routing decision
        self.path = yield self.route_decided
        self.route_decided = self.env.event()   # Reset event in case of retransmission

        # Phase 4: Wait for the controller to put flow_mod messages into switches' OFA queues.
        res = yield self.env.process(self.simcore.ctrl.flow_install_proc(self))
        if res == -1:
            logging.debug("Time %10.6f: Flow #%d failed to install entries to all path switches." %(self.env.now, self.fid))
            self.env.exit(-1)
        else:
            logging.debug("Time %10.6f: Flow #%d successfully installed entries to all path switches. Path is: %s." %(self.env.now, self.fid, self.path))

        self.env.exit(0)


    def active_proc(self):
        logging.info("Time %10.6f: Flow #%d starts transmission. Est. end time = %.6f." %(self.env.now, self.fid, self.env.now + self.duration))
        yield self.env.timeout(self.duration)
        logging.info("Time %10.6f: Flow #%d ends transmission" %(self.env.now, self.fid))
        self.env.exit(0)


    def idle_proc(self):
        yield self.env.timeout(IDLE_TIMEOUT)

        for nd in self.path:
            if self.use_amu == True:
                tar_dev = self.simcore.amus[nd]
            else:
                tar_dev = self.simcore.switches[nd]
            tar_dev.tcam_cnt -= 1   # Remove rules

        logging.info("Time %10.6f: Flow #%d ends idle timeout and removes entries from all path switches." %(self.env.now, self.fid))
        self.env.exit(0)
