import netaddr
import math

from SimCfg import *

def get_bit(ip, index):
    if index <= 0:      return None
    ret = int((ip >> (32-index) & 0x1))
    return ret


class TrieNode(object):
    def __init__(self, pnode=None, prefix=netaddr.IPNetwork('0.0.0.0/0'), prelen=0):
        self.left = None
        self.right = None
        self.parent = pnode
        self.prefix = prefix
        self.prelen = prelen
        self.verdict = False
        # Other attributes for HHH, etc.
        self.total = 0.0
        self.succ_cnt = 0.0             # For succinct HHHs


class NominalProfile(object):
    def __init__(self, ts=0.0, proftype="IP"):
        self.timestamp = ts
        self.proftype = proftype
        self.hhh_thresh_frac = HHH_THRESH_FRAC    # HHH threshold in fraction of total packet_ins
        self.thresh = 0.0               # Really applied threshold

        # For "IP" type profile
        if self.proftype == "IP":
            self.root = TrieNode()          # Root node of a full trie
            self.node_dict = {i:[] for i in range(0, 33)}   # Key: prefix length, value: list of all matching nodes

        # For "PORT" type profile
        elif self.proftype == "PORT":
            self.port_dict = {}
            self.port_hi = 0.0
            self.port_lo = 0.0

        # For "PROT" type profile:
        elif self.proftype == "PROT":
            self.prot_dict = {}


        self.hhh_dict = {}              # Recording all succinct HHHs. Key: prefix, value: succ_cnt
        self.total = 0.0


    def add_item(self, key):
        """ This will eventually build a full trie that records every input packet_in messages.
        """
        if self.proftype == "IP":
            curr_node = self.root
            for i in range(0, 33):
                curr_node.total += 1.0
                curr_node.succ_cnt += 1.0

                if i < 32:
                    myBit = get_bit(key, i+1)
                    if myBit == 0:
                        if curr_node.left == None:  # Will create new branch
                            myPrefix = netaddr.IPNetwork(key)
                            myPrefix.prefixlen = i+1
                            myPrefix = myPrefix.cidr
                            curr_node.left = TrieNode(pnode=curr_node, prefix=myPrefix, prelen=i+1)
                            self.node_dict[i+1].append(curr_node.left)
                        curr_node = curr_node.left
                    if myBit == 1:
                        if curr_node.right == None:
                            myPrefix = netaddr.IPNetwork(key)
                            myPrefix.prefixlen = i+1
                            myPrefix = myPrefix.cidr
                            curr_node.right = TrieNode(pnode=curr_node, prefix=myPrefix, prelen=i+1)
                            self.node_dict[i+1].append(curr_node.right)
                        curr_node = curr_node.right

        elif self.proftype == "PORT":
            self.total += 1.0
            if key in self.port_dict:       self.port_dict[key] += 1.0
            else:                           self.port_dict[key] = 1.0

            if key >= 1024:         self.port_hi += 1.0
            else:                   self.port_lo += 1.0

        elif self.proftype == "PROT":
            self.total += 1.0
            if key in self.prot_dict:       self.prot_dict[key] += 1.0
            else:                           self.prot_dict[key] = 1.0


    def post_proc(self):
        """ Post-process a nominal profile.
        Traverse the full trie and pick the succinct HHH nodes. Dump them to self.hhh_dict.
        """
        if self.proftype == "IP":
            self.thresh = self.hhh_thresh_frac * float(self.root.total)
            hhh_count = 0

            self.total = self.root.total

            for i in range(32, 0, -1):  # Not including root node for now
                for node in self.node_dict[i]:
                    if node.succ_cnt >= self.thresh:  # Discovered an HHH node. Report it
                        hhh_count += 1
                        self.hhh_dict[node.prefix] = node.succ_cnt
                        decrement = node.succ_cnt
                        traceup_node = node.parent
                        while traceup_node != None:
                            traceup_node.succ_cnt -= decrement
                            traceup_node = traceup_node.parent
                    del node    # Release some memory

            self.hhh_dict[self.root.prefix] = self.root.succ_cnt      # Root node must be added to the dict!
            # Clean up some memory
            self.node_dict = {}
            self.root = None

        elif self.proftype == "PORT":
            self.thresh = self.hhh_thresh_frac * float(self.total)
            for k in self.port_dict:
                if self.port_dict[k] >= self.thresh:
                    self.hhh_dict[k] = self.port_dict[k]
                    if k >= 1024:
                        self.port_hi -= self.port_dict[k]
                    else:
                        self.port_lo -= self.port_dict[k]

            self.hhh_dict["HIGH"] = self.port_hi
            self.hhh_dict["LOW"] = self.port_lo

        elif self.proftype == "PROT":
            self.thresh = self.hhh_thresh_frac * float(self.total)
            othertotal = self.total
            for k in self.prot_dict:
                if self.prot_dict[k] >= self.thresh:
                    self.hhh_dict[k] = self.prot_dict[k]
                    othertotal -= self.prot_dict[k]

            self.hhh_dict["OTHERS"] = othertotal


    def print_prof(self):
        denom = PERIOD_MEASURE_PROF * PERIOD_NOMINAL_PROF
        print "--------------------------------------------------------------------------------"
        print "Nominal Profile"
        print "Timestamp=%-10.6f" %(self.timestamp)
        print "Threshold = %f;  Total = %f (all numbers in fps)" %(self.thresh/denom, self.total/denom)
        print "--------------------------------------------------------------------------------"
        if self.proftype == "IP":
            for prefix in sorted(self.hhh_dict.keys(), key= lambda k: (k.prefixlen, k.ip)):
                print "Prefix: %-20s    count = %-10.3f\t\tRatio: %-10.3f" %(prefix.cidr, self.hhh_dict[prefix]/denom, self.hhh_dict[prefix]/self.total)
        else:
            for key in sorted(self.hhh_dict.keys(), key= lambda k: str(k)):
                print "Key: %-20s       count = %-10.3f\t\tRatio: %-10.3f" %(key, self.hhh_dict[key]/denom, self.hhh_dict[key]/self.total)
            print





class MeasuredProfile(object):
    def __init__(self, nomprof, ts=0.0):
        self.timestamp = ts
        self.nom_prof = nomprof
        self.nom_timestamp = nomprof.timestamp
        self.proftype = nomprof.proftype
        self.hhh_dict = {}
        self.total = 0.0

        # For "IP" type profile
        if self.proftype == "IP":
            self.root = TrieNode()
            self.root.verdict = True
            self.build_lpm_trie(nomprof)

        # For "PORT" and "PROT":
        else:
            self.init_prof()


    def build_lpm_trie(self, nomprof):
        for k in nomprof.hhh_dict.keys():
            ip = k.ip
            prelen = k.prefixlen
            curr_node = self.root
            for i in range(0, prelen):
                myBit = get_bit(ip, i+1)
                if myBit == 0:
                    if curr_node.left == None:  # Will create new branch
                        myPrefix = netaddr.IPNetwork(ip)
                        myPrefix.prefixlen = i+1
                        myPrefix = myPrefix.cidr
                        curr_node.left = TrieNode(pnode=curr_node, prefix=myPrefix, prelen=i+1)
                    curr_node = curr_node.left
                if myBit == 1:
                    if curr_node.right == None:  # Will create new branch
                        myPrefix = netaddr.IPNetwork(ip)
                        myPrefix.prefixlen = i+1
                        myPrefix = myPrefix.cidr
                        curr_node.right = TrieNode(pnode=curr_node, prefix=myPrefix, prelen=i+1)
                    curr_node = curr_node.right
            curr_node.verdict = True
            self.hhh_dict[k] = 0.0


    def init_prof(self):
        for k in self.nom_prof.hhh_dict.keys():
            self.hhh_dict[k] = 0.0


    def reset_prof(self, ts=0.0):
        self.timestamp = ts
        self.total = 0.0
        for k in self.hhh_dict.keys():
            self.hhh_dict[k] = 0.0


    def print_prof(self):
        denom = PERIOD_MEASURE_PROF
        print "--------------------------------------------------------------------------------"
        print "Measured Profile: Timestamp "
        print "Timestamp=%-10.6f; Nominal timestamp=%-10.6f" %(self.timestamp, self.nom_timestamp)
        print "Total count = %f (all numbers in fps)" %(self.total/denom)
        print "--------------------------------------------------------------------------------"
        if self.proftype == "IP":
            for k in sorted(self.hhh_dict.keys(), key= lambda k: (k.prefixlen, k.ip)):
                print "%-20s: %-10.3f\t\tRatio: %-10.3f" %(k, self.hhh_dict[k]/denom, self.hhh_dict[k]/self.total)
        else:
            for k in sorted(self.hhh_dict.keys(), key= lambda k: str(k)):
                print "%-20s: %-10.3f\t\tRatio: %-10.3f" %(k, self.hhh_dict[k]/denom, self.hhh_dict[k]/self.total)
        print

    def add_item(self, key):
        """ Do longest prefix matching and increment the counter.
        """
        self.total += 1.0
        if self.proftype == "IP":
            matched_prefix = self.lpm(key)
            self.hhh_dict[matched_prefix] += 1.0
        elif self.proftype == "PORT":
            if key in self.hhh_dict:    self.hhh_dict[key] += 1.0
            elif key >= 1024:           self.hhh_dict["HIGH"] += 1.0
            else:                       self.hhh_dict["LOW"] += 1.0
        elif self.proftype == "PROT":
            if key in self.hhh_dict:    self.hhh_dict[key] += 1.0
            else:                       self.hhh_dict["OTHERS"] += 1.0


    def lpm(self, ip):
        """ Longest prefix matching against the trie.
        """
        curr_node = self.root
        curr_prefix = curr_node.prefix
        for i in range(0, 32):
            myBit = get_bit(ip, i+1)
            if myBit == 0:
                if curr_node.left == None:
                    break
                else:
                    curr_node = curr_node.left
                    if curr_node.verdict == True:
                        curr_prefix = curr_node.prefix
            if myBit == 1:
                if curr_node.right == None:
                    break
                else:
                    curr_node = curr_node.right
                    if curr_node.verdict == True:
                        curr_prefix = curr_node.prefix
        return curr_prefix


class Scoreboard(object):
    def __init__(self, meaprof, ts=0.0):
        self.timestamp = ts
        self.proftype = meaprof.proftype
        self.mea_prof = meaprof
        self.nom_prof = meaprof.nom_prof
        self.nom_timestamp = self.nom_prof.timestamp
        self.mea_timestamp = self.mea_prof.timestamp
        self.score_dict = {}
        self.levelscore = 0.0

        if self.proftype == "IP":
            self.root = TrieNode()
            self.root.verdict = True
            self.build_lpm_trie(self.nom_prof)

        for k in self.nom_prof.hhh_dict.keys():
            nom_value = self.nom_prof.hhh_dict[k]
            mea_value = self.mea_prof.hhh_dict[k]
            if mea_value == 0.0:
                mea_value = 1.0         # Set to a small value
            if nom_value == 0.0:
                self.score_dict[k] = float('-inf')      # Should not have any match. Just in case.
                continue
            self.score_dict[k] = math.log(nom_value/self.nom_prof.total) - math.log(mea_value/self.mea_prof.total)  # Dictionary in a score

        denom1 = PERIOD_MEASURE_PROF*PERIOD_NOMINAL_PROF
        denom2 = PERIOD_MEASURE_PROF
        self.levelscore = math.log(self.nom_prof.total/denom1) - math.log(self.mea_prof.total/denom2)


    def build_lpm_trie(self, nomprof):
        for k in nomprof.hhh_dict.keys():
            ip = k.ip
            prelen = k.prefixlen
            curr_node = self.root
            for i in range(0, prelen):
                myBit = get_bit(ip, i+1)
                if myBit == 0:
                    if curr_node.left == None:  # Will create new branch
                        myPrefix = netaddr.IPNetwork(ip)
                        myPrefix.prefixlen = i+1
                        myPrefix = myPrefix.cidr
                        curr_node.left = TrieNode(pnode=curr_node, prefix=myPrefix, prelen=i+1)
                    curr_node = curr_node.left
                if myBit == 1:
                    if curr_node.right == None:  # Will create new branch
                        myPrefix = netaddr.IPNetwork(ip)
                        myPrefix.prefixlen = i+1
                        myPrefix = myPrefix.cidr
                        curr_node.right = TrieNode(pnode=curr_node, prefix=myPrefix, prelen=i+1)
                    curr_node = curr_node.right
            curr_node.verdict = True
            self.score_dict[k] = 0


    def print_score(self):
        print "--------------------------------------------------------------------------------"
        print "Scoreboard"
        print "Timestamp= %-10.6f; Nominal timestamp=%-10.6f; Measured timestamp=%-10.6f" %(self.timestamp, self.nom_timestamp, self.mea_timestamp)
        print "Level score = %f" %(self.levelscore)
        print "--------------------------------------------------------------------------------"
        for k in sorted(self.score_dict.keys(), key= lambda k: (k.prefixlen, k.ip)):
            print "%-20s: %-10.3f" %(k, self.score_dict[k])
        print


    def get_score(self, key):
        if self.proftype == "IP":
            prefix = self.lpm(key)
            return self.score_dict[prefix]
        elif self.proftype == "PORT":
            if key in self.score_dict:  return self.score_dict[key]
            elif key >= 1024:           return self.score_dict["HIGH"]
            else:                       return self.score_dict["LOW"]
        elif self.proftype == "PROT":
            if key in self.score_dict:  return self.score_dict[key]
            else:                       return self.score_dict["OTHERS"]



    def lpm(self, ip):
        """ Longest prefix matching against the trie.
        """
        curr_node = self.root
        curr_prefix = curr_node.prefix
        for i in range(0, 32):
            myBit = get_bit(ip, i+1)
            if myBit == 0:
                if curr_node.left == None:
                    break
                else:
                    curr_node = curr_node.left
                    if curr_node.verdict == True:
                        curr_prefix = curr_node.prefix
            if myBit == 1:
                if curr_node.right == None:
                    break
                else:
                    curr_node = curr_node.right
                    if curr_node.verdict == True:
                        curr_prefix = curr_node.prefix
        return curr_prefix


class CDF(object):
    def __init__(self, ts=0.0):
        self.timestamp      = ts
        self.score_list     = []
        self.input_load     = 0.0
        self.output_load    = 0.0
        self.target_load    = TGT_GLOBAL_LOAD
        self.permit_frac    = 1.0
        self.pi_load_ratio  = 1.0
        self.thresh_score   = 0.0


    def add_item(self, score, sender):
        self.score_list.append(score)
        if sender == "CTRL":
            self.input_load += 1.0/PERIOD_SCOREBOARD
            self.output_load += 1.0/PERIOD_SCOREBOARD
        elif sender == "AMU_FILTER":
            self.input_load += 1.0/PERIOD_SCOREBOARD


    def calc_threshold(self):
        # Load shedding
        self.pi_load_ratio *= self.target_load / self.output_load
        self.permit_frac = min(self.pi_load_ratio, 1.0)
        self.permit_frac = max(self.permit_frac, MIN_PERMIT_LOAD_FRAC)  # Prevent all-blocking
        self.permit_frac = min(self.target_load/self.input_load, 1.0)
        # Lookup
        self.score_list = sorted(self.score_list)
        llen = len(self.score_list)
        idx1 = int(math.ceil(float(llen) * (1-self.permit_frac)))
        idx2 = int(math.floor(float(llen) * (1-self.permit_frac)))
        self.thresh_score  = 0.5 * (self.score_list[idx1] + self.score_list[idx2])
        self.score_list = []      # Clean up to avoid space waste
