#!/usr/bin/python

#/home/cgi0911/PyPyEnv/bin/pypy

# -----------------------------------------------------------------------------
#
# Imports
#
# -----------------------------------------------------------------------------
import simpy
import random
import netaddr
import os
import networkx as nx
import csv
import time
import logging

from SimCfg import *
from SimCore import *
from SimCtrl import *
from SimFlow import *
from SimFlowGen import *
from SimMsg import *
from SimSwitch import *

logging.basicConfig(filename='log/log_%s.txt' %(time.strftime("%Y%m%d-%H%M%S")), level=logging.WARNING )
#logging.basicConfig(level=logging.DEBUG )

# -----------------------------------------------------------------------------
#
# Main Course
#
# -----------------------------------------------------------------------------
def main_course():
    logging.critical("---------- Start of execution ----------")
    random.seed(time.time())

    #h = hpy()
    # Instantiate simpy environment
    env = simpy.Environment(initial_time=SIM_INIT_TIME)

    # Build simcore and add components.
    simcore = SimCore(env)
    simcore.add_monitor()
    simcore.add_ctrl()
    simcore.add_switches()
    simcore.add_amus()
    simcore.add_flowgen(forged=False)
    simcore.add_flowgen(forged=True)

    # Start simulation run
    env.run(until=SIM_INIT_TIME+SIM_TIME+1e-6)  # A very small increment to ensure simulation complete.

    #print h.heap()
    logging.critical("---------- End of execution ----------")



if __name__ == "__main__":
    main_course()
