import simpy
import logging

from SimCfg import *
from SimFlow import *
from SimMsg import *
from SimMonitor import *

class SimSwitch(object):
    # Class for an OpenFlow switch object
    def __init__(self, env, simcore, name="SW0"):
        self.env = env
        self.simcore = simcore
        self.monitor = simcore.monitor
        self.name = name
        # Resources
        self.ofa_q = simpy.Store(env, capacity=SW_OFA_Q_LEN)       # Model an OpenFlow agent buffer
        #self.tcam = {}        # Model a limited-size TCAM table. A dict hashed by 5-tuple.
        self.tcam_cnt = 0       # Record TCAM occupancy
        self.tcam_size = TCAM_SIZE

        env.process(self.ofa_proc())


    def __str__(self):
        return "Switch_%s" %(self.name)


    def __repr__(self):
        return self.__str__()


    def put_flowarriv_proc(self, flow):
        """Put flow arrival to the switch's OFA queue.
        Exit with -1 if fail to put (due to ofa_q overflow).
        """
        yield self.env.timeout(PROP_DELAY1)              # Model the propagation delay

        #if flow.legit == False:
        #    yield self.env.exit(-1)

        with self.ofa_q.put(flow) as put_req:
            self.monitor.sw_load_dict[self.name] += 1
            logging.debug("Time %10.6f: %s trying to put the arrival of Flow #%d to ofa_q." %(self.env.now, self.name, flow.fid))
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue is full

        if not put_req in res:
            logging.debug("Time %10.6f: %s failed to put the flow arrival of Flow #%d to ofa_q." %(self.env.now, self.name, flow.fid))
            self.env.exit(-1)   # Return a failure flag
        else:
            logging.debug("Time %10.6f: %s successfully put the flow arrival of Flow #%d to ofa_q. Current queue occupancy = %d." \
                          %(self.env.now, self.name, flow.fid, len(self.ofa_q.items)) )
            self.env.exit(0)    # Return a success flag


    def put_flowmod_proc(self, msg):
        """Put flow mod to the switch's OFA queue.
        Exit with -1 if fail to put (due to ofa_q overflow).
        """
        yield self.env.timeout(PROP_DELAY2)             # Model the propagation delay

        #flow = msg.flowobj

        with self.ofa_q.put(msg) as put_req:
            #self.monitor.sw_load_dict[self.name] += 1
            logging.debug("Time %10.6f: %s trying to put the flow_mod message of Flow #%d to ofa_q." %(self.env.now, self.name, msg.fid))
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue is full

        if not put_req in res:
            logging.debug("Time %10.6f: %s failed to put the flow_mod message of Flow #%d to ofa_q." %(self.env.now, self.name, msg.fid))
            msg.installed.succeed(value=-1)
            self.env.exit(-1)   # Return a failure flag
        else:
            logging.debug("Time %10.6f: %s successfully put the flow_mod message of Flow #%d to ofa_q. Current queue occupancy = %d." \
                          %(self.env.now, self.name, msg.fid, len(self.ofa_q.items)) )
            self.env.exit(0)


    def ofa_proc(self):
        """Process of OpenFlow agent handling OpenFlow events.
        """
        while True:     # Continuously process events
            ofa_q_item = yield self.ofa_q.get()
            logging.debug("Time %10.6f: %s gets an event from ofa_q. Event = %s." %(self.env.now, self.name, ofa_q_item))
            if type(ofa_q_item) == SimFlow:
                yield self.env.timeout(SW_PROC_TIME_FLOWARRIV)
                msg = SimMsg_PacketIn(self.env, ofa_q_item)
                ofa_q_item.sw_ofa_processed.succeed(value=msg)
                # No further processing. Controller actions will be initiated by SimFlow object.
            elif type(ofa_q_item) == SimMsg_FlowMod:
                #flow = ofa_q_item.flowobj
                node_name = self.name
                if self.tcam_cnt >= self.tcam_size:
                    logging.debug("Time %10.6f: %s do not have TCAM space for new entry of Flow #%d." %(self.env.now, self.name, ofa_q_item.fid))
                    ofa_q_item.installed.succeed(value=-1)
                    #flow.tcam_have_space[self.name].succeed(value=-1)
                else:
                    logging.debug("Time %10.6f: %s have TCAM space for new entry of Flow #%d." %(self.env.now, self.name, ofa_q_item.fid))
                    yield self.env.timeout(SW_PROC_TIME_FLOWMOD)
                    #self.tcam[flow.fid] = 1
                    #self.tcam_cnt += 1     # incremented by SimCtrl
                    logging.debug("Time %10.6f: %s installed new entry of Flow #%d." %(self.env.now, self.name, ofa_q_item.fid))
                    try:
                        ofa_q_item.installed.succeed(value=0)
                    except:
                        pass    # This part must be debugged!
