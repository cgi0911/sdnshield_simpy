import simpy
import logging
import copy

from SimCfg import *
from SimMsg import *
from SimPktScore import *

class SimCtrl(object):
    # Class for a centralized controller
    def __init__(self, env, simcore):
        self.env = env
        self.simcore = simcore
        # Resources - controller input queue
        self.input_q = simpy.Store(env, capacity=CTRL_INPUT_Q_LEN)
        # Profiles and related parameters
        self.dict_nom_prof_srcip            = {}
        self.dict_nom_prof_dstip            = {}
        self.dict_nom_prof_srcport          = {}
        self.dict_nom_prof_dstport          = {}
        self.dict_nom_prof_prot             = {}

        self.dict_mea_prof_srcip            = {}
        self.dict_mea_prof_dstip            = {}
        self.dict_mea_prof_srcport          = {}
        self.dict_mea_prof_dstport          = {}
        self.dict_mea_prof_prot             = {}


        self.dict_scoreboard_srcip          = {}
        self.dict_scoreboard_dstip          = {}
        self.dict_scoreboard_srcport        = {}
        self.dict_scoreboard_dstport        = {}
        self.dict_scoreboard_prot           = {}

        self.dict_cdf                       = {}

        self.working_nom_profile_srcip      = None
        self.working_nom_profile_dstip      = None
        self.working_nom_profile_srcport    = None
        self.working_nom_profile_dstport    = None
        self.working_nom_profile_prot       = None

        self.ready_nom_profile_srcip        = None
        self.ready_nom_profile_dstip        = None
        self.ready_nom_profile_srcport      = None
        self.ready_nom_profile_dstport      = None
        self.ready_nom_profile_prot         = None

        self.working_mea_profile_srcip      = None
        self.working_mea_profile_dstip      = None
        self.working_mea_profile_srcport    = None
        self.working_mea_profile_dstport    = None
        self.working_mea_profile_prot       = None

        self.ready_mea_profile_srcip        = None
        self.ready_mea_profile_dstip        = None
        self.ready_mea_profile_srcport      = None
        self.ready_mea_profile_dstport      = None
        self.ready_mea_profile_prot         = None

        self.working_scoreboard_srcip       = None
        self.working_scoreboard_dstip       = None
        self.working_scoreboard_srcport     = None
        self.working_scoreboard_dstport     = None
        self.working_scoreboard_prot        = None

        self.ready_scoreboard_srcip         = None
        self.ready_scoreboard_dstip         = None
        self.ready_scoreboard_srcport       = None
        self.ready_scoreboard_dstport       = None
        self.ready_scoreboard_prot          = None

        self.working_cdf                    = None
        self.ready_cdf                      = None

        self.permit_frac = 1.0          # See PacketScore paper
        self.multiplied_phi = 1.0       # See PacketScore paper
        self.curr_load = 0.0            # Load (# of packet_in msgs) measured in this time slot
        # Flags and simpy events
        self.nom_prof_updated               = self.env.event()
        self.mea_prof_updated               = self.env.event()
        self.scoreboard_updated             = self.env.event()
        self.loadcheck_cnt = 0.0       # For load check module


    def put_proc(self, msg):
        """Put packet_in message to the controller's input queue.
        """
        yield self.env.timeout(PROP_DELAY2)

        with self.input_q.put(msg) as put_req:
            # Per-packet_in tracking
            self.curr_load += 1.0
            self.loadcheck_cnt += 1.0
            # Add to nominal profile
            self.working_nom_profile_srcip.add_item(msg.src_ip)
            self.working_nom_profile_dstip.add_item(msg.dst_ip)
            self.working_nom_profile_srcport.add_item(msg.src_port)
            self.working_nom_profile_dstport.add_item(msg.dst_port)
            self.working_nom_profile_prot.add_item(msg.prot)
            # Add to measured profile
            if self.working_mea_profile_srcip != None:
                self.working_mea_profile_srcip.add_item(msg.src_ip)
                self.working_mea_profile_dstip.add_item(msg.dst_ip)
                self.working_mea_profile_srcport.add_item(msg.src_port)
                self.working_mea_profile_dstport.add_item(msg.dst_port)
                self.working_mea_profile_prot.add_item(msg.prot)
            # Add to scorebook
            if self.working_scoreboard_srcip != None and self.working_scoreboard_dstip != None:
                srcip_score = self.working_scoreboard_srcip.get_score(msg.src_ip)
                dstip_score = self.working_scoreboard_dstip.get_score(msg.dst_ip)
                srcport_score = self.working_scoreboard_srcport.get_score(msg.src_port)
                dstport_score = self.working_scoreboard_dstport.get_score(msg.dst_port)
                prot_score = self.working_scoreboard_prot.get_score(msg.prot)
                total_score = srcip_score + dstip_score + srcport_score + dstport_score + prot_score + self.working_scoreboard_srcip.levelscore
                self.working_cdf.add_item(total_score, 'CTRL')


            logging.debug("Time %10.6f: Controller trying to put message to input_q. Message = %s" %(self.env.now, msg))
            res = yield put_req | self.env.timeout(0.0)     # No tolerance if queue if full

        if not put_req in res:
            logging.debug("Time %10.6f: Controller failed to put message to input_q. Message = %s" %(self.env.now, msg))
            self.env.exit(-1)
        else:
            logging.debug("Time %10.6f: Controller successfully put message to input_q. Message = %s" %(self.env.now, msg))
            self.env.exit(0)


    def routing_proc(self):
        # Infinite loop that continuously pops packet_in messages and do routing calculation
        while True:
            msg = yield self.input_q.get()
            logging.debug("Time %10.6f: Controller gets a message from input_q. Message = %s." %(self.env.now, msg))

            # Do the routing (shortest path only)
            yield self.env.timeout(CTRL_PROC_TIME_ROUTING)  # Model the processing

            if msg.use_amu == True:
                path = [msg.src_node]        # Use only the AMU near the source
            else:
                src = msg.src_node
                dst = msg.dst_node
                #path = self.simcore.path_db[(src, dst)][0]
                # Assume every pair of nodes is directly connected!
                if src == dst:
                    path = [src]    # Avoid duplicated delete item
                else:
                    path = [src, dst]

            yield msg.route_decided.succeed(value=path)


    def flow_install_proc(self, flow):
        # Install flow_mod messages at each switch
        ev_list = []
        ev_dict = {}
        msg_list = []

        for nd in flow.path:
            if flow.use_amu == True:
                tar_dev = self.simcore.amus[nd]
            else:
                tar_dev = self.simcore.switches[nd]
            msg = SimMsg_FlowMod(self.env, flow)
            ev_list.append(msg.installed) # Add 'installed' event to event list
            self.env.process(tar_dev.put_flowmod_proc(msg))  # Add a put_flowmod_proc

        res = yield simpy.events.AllOf(self.env, ev_list)

        if all(res[k] == 0 for k in res):
            for nd in flow.path:
                if flow.use_amu == True:
                    tar_dev = self.simcore.amus[nd]
                else:
                    tar_dev = self.simcore.switches[nd]
                tar_dev.tcam_cnt += 1    # Increment the counter, which means occupy a rule space

            self.env.exit(0)  # Return a successful flag
        else:
            self.env.exit(-1)


    def nom_prof_proc(self):
        idx = self.env.now
        while True:
            self.working_nom_profile_srcip = self.dict_nom_prof_srcip[idx] = NominalProfile(ts=self.env.now, proftype="IP")
            self.working_nom_profile_dstip = self.dict_nom_prof_dstip[idx] = NominalProfile(ts=self.env.now, proftype="IP")
            self.working_nom_profile_srcport = self.dict_nom_prof_srcport[idx] = NominalProfile(ts=self.env.now, proftype="PORT")
            self.working_nom_profile_dstport = self.dict_nom_prof_dstport[idx] = NominalProfile(ts=self.env.now, proftype="PORT")
            self.working_nom_profile_prot = self.dict_nom_prof_prot[idx] = NominalProfile(ts=self.env.now, proftype="PROT")
            yield self.env.timeout(PERIOD_NOMINAL_PROF * PERIOD_MEASURE_PROF)
            self.working_nom_profile_srcip.post_proc()
            self.working_nom_profile_dstip.post_proc()
            self.working_nom_profile_srcport.post_proc()
            self.working_nom_profile_dstport.post_proc()
            self.working_nom_profile_prot.post_proc()
            ready_nom_idx = idx


            print "Time %-10.6f: Nominal profile complete and archived. Profile timestamp = %-10.6f" %(self.env.now, self.dict_nom_prof_srcip[ready_nom_idx].timestamp)
            #self.dict_nom_prof_srcip[ready_nom_idx].print_prof()
            #self.dict_nom_prof_dstip[ready_nom_idx].print_prof()
            #print
            #print
            idx = self.env.now
            self.nom_prof_updated.succeed( value=ready_nom_idx )


    def mea_prof_proc(self):
        while True:
            ready_nom_idx = yield self.nom_prof_updated
            ready_nom_profile_srcip = self.dict_nom_prof_srcip[ready_nom_idx]
            ready_nom_profile_dstip = self.dict_nom_prof_dstip[ready_nom_idx]
            ready_nom_profile_srcport = self.dict_nom_prof_srcport[ready_nom_idx]
            ready_nom_profile_dstport = self.dict_nom_prof_dstport[ready_nom_idx]
            ready_nom_profile_prot = self.dict_nom_prof_prot[ready_nom_idx]
            self.nom_prof_updated = self.env.event()
            for i in range(PERIOD_NOMINAL_PROF):
                idx = self.env.now
                self.working_mea_profile_srcip = self.dict_mea_prof_srcip[idx] = MeasuredProfile(ready_nom_profile_srcip, ts=self.env.now)
                self.working_mea_profile_dstip = self.dict_mea_prof_dstip[idx] = MeasuredProfile(ready_nom_profile_dstip, ts=self.env.now)
                self.working_mea_profile_srcport = self.dict_mea_prof_srcport[idx] = MeasuredProfile(ready_nom_profile_srcport, ts=self.env.now)
                self.working_mea_profile_dstport = self.dict_mea_prof_dstport[idx] = MeasuredProfile(ready_nom_profile_dstport, ts=self.env.now)
                self.working_mea_profile_prot = self.dict_mea_prof_prot[idx] = MeasuredProfile(ready_nom_profile_prot, ts=self.env.now)
                yield self.env.timeout(PERIOD_MEASURE_PROF)
                ready_mea_idx = idx


                print "Time %-10.6f: Measured profile complete and archived. Profile timestamp = %-10.6f; Structured from nom_timestamp = %-10.6f" \
                      %(self.env.now, self.dict_mea_prof_srcip[ready_mea_idx].timestamp, self.dict_mea_prof_srcip[ready_mea_idx].nom_prof.timestamp)
                #self.dict_mea_prof_srcip[ready_mea_idx].print_prof()
                #self.dict_mea_prof_dstip[ready_mea_idx].print_prof()
                #print "\n\n"

                self.mea_prof_updated.succeed( value=ready_mea_idx )


    def scoreboard_proc(self):
        while True:
            ready_mea_idx = yield self.mea_prof_updated
            ready_mea_profile_srcip = self.dict_mea_prof_srcip[ready_mea_idx]
            ready_mea_profile_dstip = self.dict_mea_prof_dstip[ready_mea_idx]
            ready_mea_profile_srcport = self.dict_mea_prof_srcport[ready_mea_idx]
            ready_mea_profile_dstport = self.dict_mea_prof_dstport[ready_mea_idx]
            ready_mea_profile_prot = self.dict_mea_prof_prot[ready_mea_idx]
            self.mea_prof_updated = self.env.event()

            for i in range(int(PERIOD_MEASURE_PROF/PERIOD_SCOREBOARD)):
                idx = self.env.now
                self.working_scoreboard_srcip = self.dict_scoreboard_srcip[idx] = Scoreboard(ready_mea_profile_srcip, ts=self.env.now)
                self.working_scoreboard_dstip = self.dict_scoreboard_dstip[idx] = Scoreboard(ready_mea_profile_dstip, ts=self.env.now)
                self.working_scoreboard_srcport = self.dict_scoreboard_srcport[idx] = Scoreboard(ready_mea_profile_srcport, ts=self.env.now)
                self.working_scoreboard_dstport = self.dict_scoreboard_dstport[idx] = Scoreboard(ready_mea_profile_dstport, ts=self.env.now)
                self.working_scoreboard_prot = self.dict_scoreboard_prot[idx] = Scoreboard(ready_mea_profile_prot, ts=self.env.now)
                self.working_cdf              = self.dict_cdf[idx]              = CDF(ts=self.env.now)
                yield self.env.timeout(PERIOD_SCOREBOARD)
                # CDF analysis and threshold update on working_cdf!
                self.working_cdf.calc_threshold()
                # Enter filter mode as soon as there is an overload detected
                if self.simcore.op_mode != "FILTER" and self.working_cdf.permit_frac < 1.0:
                    if FILTER_MODE == True:
                        self.simcore.op_mode = "FILTER"
                        print "Time %-10.6f: Operation mode switched to FILTER" %(self.env.now)
                elif self.simcore.op_mode != "NORMAL" and self.working_cdf.permit_frac >= 1.0:
                    self.simcore.op_mode = "NORMAL"
                    print "Time %-10.6f: Operation mode switched to NORMAL" %(self.env.now)
                ready_scoreboard_idx = idx
                self.ready_scoreboard_srcip = self.working_scoreboard_srcip
                self.ready_scoreboard_dstip = self.working_scoreboard_dstip
                self.ready_scoreboard_srcport = self.working_scoreboard_srcport
                self.ready_scoreboard_dstport = self.working_scoreboard_dstport
                self.ready_scoreboard_prot = self.working_scoreboard_prot
                self.ready_cdf              = self.working_cdf

                print "Time %-10.6f: Scoreboard archived. Profile timestamp = %-10.6f; Structured from nom/mea=%-10.6f/%-10.6f" \
                      %(self.env.now, self.dict_scoreboard_srcip[ready_scoreboard_idx].timestamp, \
                        self.dict_scoreboard_srcip[ready_scoreboard_idx].nom_timestamp, \
                        self.dict_scoreboard_srcip[ready_scoreboard_idx].mea_timestamp)
                print "Permit fraction=%f; threshold score=%f" %(self.working_cdf.permit_frac, self.working_cdf.thresh_score)
                print "Input load=%f; output load=%f" %(self.working_cdf.input_load, self.working_cdf.output_load)
                #self.dict_scoreboard_srcip[ready_scoreboard_idx].print_score()
                #self.dict_scoreboard_dstip[ready_scoreboard_idx].print_score()
                print "\n\n"


    def load_check_proc(self):
        yield self.env.timeout(60.0)
        while True:
            yield self.env.timeout(PERIOD_LOAD_CHECK)

            #print "Time %-10.6f: Load = %f fps" %(self.env.now, self.loadcheck_cnt / PERIOD_LOAD_CHECK)

            if SUSTAIN_MODE == True:
                if self.simcore.op_mode == "NORMAL":
                    if self.loadcheck_cnt >= TGT_GLOBAL_LOAD * PERIOD_LOAD_CHECK:   # Overload
                        self.simcore.op_mode = "SUSTAIN"
                        print "Time %-10.6f: Operation mode switched to SUSTAIN; Load = %f fps" %(self.env.now, self.loadcheck_cnt / PERIOD_LOAD_CHECK)

            self.loadcheck_cnt = 0.0    # Reset counter