import random
import netaddr

from SimCfg import *
from SimFlow import *

class SimFlowGen(object):
    """ Class for flow generator. Can be instantiated to simulate DDoS attacks by generating forged flows.
    To forge a mixed attack, you can create multiple instances.
    """
    curr_fid = 0        # Class variable. Shared by all flow generator instances.

    def __init__(self, env, simcore, forged=True, fn_flows=FN_FLOWS):
        self.env = env
        self.simcore = simcore
        #self.curr_fid = 0       # Used to assign FIDs to flows
        self.node_list = sorted(self.simcore.topo.nodes())
        self.n_nodes = len(self.node_list)
        self.gen_forged = forged
        self.fn_flows = fn_flows

        # Add process to env
        if self.gen_forged == False:
            self.env.process(self.legit_flowgen_proc())
        if self.gen_forged == True:
            self.env.process(self.forged_flowgen_proc())


    def parse_flow_rec(self, flow_rec):
        ret = {}
        fields = flow_rec.split()
        ret['src_ip'] = netaddr.IPAddress(fields[0])
        ret['dst_ip'] = netaddr.IPAddress(fields[1])
        ret['prot'] = int(fields[2])
        ret['src_port'] = int(fields[3])
        ret['dst_port'] = int(fields[4])
        ret['n_pkts'] = float(fields[5])
        ret['arrival_time'] = float(fields[6])
        ret['end_time'] = float(fields[7])
        ret['duration'] = float(fields[8])
        #src_idx = hash(int(ret['src_ip'] >> 16)) % self.n_nodes
        #dst_idx = hash(int(ret['dst_ip'] >> 16)) % self.n_nodes
        #ret['src_node'] = self.node_list[src_idx]
        #ret['dst_node'] = self.node_list[dst_idx]
        #ret['src_node'] = random.choice(self.node_list)
        #ret['dst_node'] = random.choice(self.node_list)
        ret['src_node'] = self.node_list[SimFlowGen.curr_fid % len(self.node_list)]
        ret['dst_node'] = self.node_list[(SimFlowGen.curr_fid+1) % len(self.node_list)]
        return ret


    def legit_flowgen_proc(self):
        with open(self.fn_flows) as flow_file:
            curr_flow = self.parse_flow_rec(flow_file.next())
            curr_flow_arr = curr_flow['arrival_time']
            yield self.env.timeout(curr_flow_arr - SIM_INIT_TIME)
            while True:
                new_flow = SimFlow(self.env, self.simcore, src_ip=curr_flow['src_ip'],
                                   dst_ip=curr_flow['dst_ip'], src_port=curr_flow['src_port'],
                                   dst_port=curr_flow['dst_port'], prot=curr_flow['prot'],
                                   fid=SimFlowGen.curr_fid, src_node=curr_flow['src_node'],
                                   dst_node=curr_flow['dst_node'], legit=True, max_flowreqs=MAX_N_FLOWREQS)

                # if OVERRIDE_FLOW_DURATION == False:
                #     if curr_flow['duration'] == 0.0:
                #         new_flow.duration = FLOW_DURATION
                #     else:
                #         new_flow.duration = curr_flow['duration']
                # else:
                #     new_flow.duration = FLOW_DURATION       # Always overwrite flow duration

                self.env.process(new_flow.life_proc())
                SimFlowGen.curr_fid += 1

                # Try to get next flow record
                try:
                    next_flow = self.parse_flow_rec(flow_file.next())
                    next_flow_arr = next_flow['arrival_time']
                    intarr_time = next_flow_arr - curr_flow_arr
                    yield self.env.timeout(intarr_time)
                    curr_flow = next_flow
                    curr_flow_arr = curr_flow['arrival_time']

                except:
                    break   # End of flow file



    def forged_flowgen_proc(self):
        yield self.env.timeout(ATTACK_START_TIME)
        while True:
            intarr_time = random.uniform(0, 2*AVG_INTARR_TIME)
            yield  self.env.timeout(intarr_time)
            self.gen_a_forged_flow()


    def gen_a_forged_flow(self):
        while True:
            src_ip_lo = int(netaddr.IPAddress("150.0.0.0"))
            src_ip_hi = int(netaddr.IPAddress("150.255.255.255"))
            dst_ip_lo = int(netaddr.IPAddress("25.0.0.0"))
            dst_ip_hi = int(netaddr.IPAddress("25.255.255.255"))
            #src_ip = netaddr.IPAddress(random.randint(0, 2**32))
            #dst_ip = netaddr.IPAddress(random.randint(0, 2**32))
            src_ip = netaddr.IPAddress(random.randint(src_ip_lo, src_ip_hi))
            dst_ip = netaddr.IPAddress(random.randint(dst_ip_lo, dst_ip_hi))
            src_idx = hash(int(src_ip >> 16)) % self.n_nodes
            dst_idx = hash(int(dst_ip >> 16)) % self.n_nodes
            if (src_ip <> dst_ip and src_idx <> dst_idx):
                src_node = self.node_list[src_idx]
                dst_node = self.node_list[dst_idx]
                break

        while True:
            #src_port = random.randint(0, 65535)
            #dst_port = random.randint(0, 65535)
            src_port = random.randint(1024, 65535)
            dst_port = random.randint(1024, 65535)
            if (src_port <> dst_port):
                break

        prot = random.choice([1, 6, 17])

        new_flow =   SimFlow(self.env, self.simcore, src_ip=src_ip, dst_ip=dst_ip,
                     src_port=src_port, dst_port=dst_port, prot=prot, duration=0.0,
                     fid=SimFlowGen.curr_fid, src_node=src_node, dst_node=dst_node, legit=False, max_flowreqs=1)
        #print "Time %10.6f: Flow generator send new flow. %s" %(self.env.now, new_flow)
        self.env.process(new_flow.life_proc())

        SimFlowGen.curr_fid += 1