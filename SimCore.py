import networkx as nx
import csv
import simpy

from SimCfg import *
from SimCtrl import *
from SimFlow import *
from SimSwitch import *
from SimFlowGen import *
from SimMonitor import *
from SimAMU import *

class SimCore(object):
    """ Class for main process and entire simulation database (except those recorded in simpy.Environment).
    """
    def __init__(self, env):
        # Environment
        self.env = env
        # Operation mode - "NORMAL", "SUSTAIN" or "FILTER"
        self.op_mode = INIT_OP_MODE
        # Topology (as an NetworkX undirected graph)
        self.topo = self.build_topo(fn_nodes=FN_NODES, fn_links=FN_LINKS)
        self.path_db = self.build_paths()
        # A dict of switches
        self.switches = {}
        self.amus = {}
        # Controller
        self.ctrl = None
        # Flow generator
        self.flowgen = None
        # Monitor
        self.monitor = None


    def add_monitor(self):
        self.monitor = SimMonitor(self.env, self)
        self.env.process(self.monitor.print_proc())

    def add_ctrl(self):
        self.ctrl = SimCtrl(self.env, self)
        self.env.process(self.ctrl.routing_proc())  # Activate the routing process
        #self.env.process(self.ctrl.prof_proc())
        self.env.process(self.ctrl.nom_prof_proc())
        self.env.process(self.ctrl.mea_prof_proc())
        self.env.process(self.ctrl.scoreboard_proc())
        self.env.process(self.ctrl.load_check_proc())


    def add_switches(self):
        for node_name in self.topo.nodes():
            self.switches[node_name] = SimSwitch(self.env, self, name=node_name)


    def add_amus(self):
        for node_name in self.topo.nodes():
            self.amus[node_name] = SimAMU(self.env, self, name=node_name)


    def add_flowgen(self, forged=False):
        self.flowgen = SimFlowGen(self.env, self, forged=forged)


    def build_topo(self, fn_nodes=FN_NODES, fn_links=FN_LINKS):
        """Build topology - read nodes and links from csv files and build topology info
        in the format of NetworkX undirected graph.
        """
        def dict_convert(myDict):
            # Each row of the CSV files is parsed into a row dictionary. Dict keys are
            # column names and values are assigned accordingly, but in string format.
            # This function converts strings into corresponding integers or float numbers.
            ret = {}
            for k in myDict:
                try:
                    ret[k] = int(myDict[k])
                except:
                    try:
                        ret[k] = float(myDict[k])
                    except:
                        ret[k] = myDict[k]
            return ret

        topo = nx.Graph()

        nodes_csv_reader = csv.DictReader(open(fn_nodes, 'rU'))
        links_csv_reader = csv.DictReader(open(fn_links, 'rU'))

        # Read nodes file row by row
        for rowdict in nodes_csv_reader:
            rowdict = dict_convert(rowdict)
            name = rowdict['name']
            topo.add_node(name)

        # Read links file row by row
        for rowdict in links_csv_reader:
            rowdict = dict_convert(rowdict)
            node1, node2 = rowdict['node1'], rowdict['node2']
            topo.add_edge(node1, node2)

        return topo


    def build_paths(self):
        """For each source/destination pair, pre-build its shortest path.
        """
        path_db = {}
        node_list = self.topo.nodes()

        for src in node_list:
            for dst in node_list:
                paths = []
                paths.append( nx.shortest_path(self.topo, src, dst) )
                path_db[(src, dst)] = paths

        return path_db